package com.MyDollarDays.CommonMethods;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class MyDollarDaysCommonMethods {

	public static Properties Prop = new Properties();
	WebDriver driver = null;
	
	public static void captureScreenShot(WebDriver driver,String FileName,String folder) throws IOException{
			// Take screenshot and store as a file format             
			 File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);  
	 		try {
			// now copy the  screenshot to desired location using copyFile method		 
			FileUtils.copyFile(src, new File("C:\\JavaPrograms\\MyDollarDaysProject\\src\\test\\java\\com\\MyDollarDays\\CommonMethods"+folder+"\\"+FileName+".png"));                              }
			catch (IOException e)		 
			{
			  System.out.println(e.getMessage()); 
			}
		}
		

	public void LoadProperties() throws IOException {
		InputStream ip = getClass().getClassLoader().getResourceAsStream("config.properties");
		Prop.load(ip);// load the properties from property file in to ip
		System.out.println("loaded");
	}

	public WebDriver OpeningWebsite() throws IOException 
	{
	  try 
		{
			
			LoadProperties();																		
			String browserName = Prop.getProperty("browser");
			if (browserName.equals("chrome")) {
				/*System.setProperty("webdriver.chrome.driver",
						"C:\\Users\\Chenchu\\Downloads\\chromedriver_win32\\chromedriver.exe"); */
				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();
			}

			else if (browserName.contentEquals("FF")) {
				//System.setProperty("webdriver.gecko.driver", "C:\\geckodriver.exe");
				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();// launch firefox browser

			}

			driver.get(Prop.getProperty("url"));
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().window().maximize();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return driver;
	}

	}

