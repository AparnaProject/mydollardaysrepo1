package com.MyDollarDays.Weblocators;

import com.MyDollarDays.CommonMethods.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MyDollarDaysWeblocators extends MyDollarDaysCommonMethods{

	private static final String SIGN_IN_IMG_XPATH = "//*[contains(@class,'header-user')]";
	public static final String SIGNIN_LINK_LINKTEXT = "Sign In";
	public static final String EMAILADDRESS_XPATH = "//*[contains(@id,'inputLoginUsername')]";
	public static final String PASSWORD_XPATH = "//*[contains(@id,'inputLoginPassword')]";
	public static final String SIGNIN_BTN_XPATH = "//*[@class='btn' and @type='submit']";
	public static final String SubmitARequest_Link_LinkText ="Submit A Request";
	public static final String Email_TxtBox_SubmitRequestPage_Xpath = "//*[contains(@id,'ct100_cphContent_txtemail')]";
	public static final String Phone_TxtBox_SubmitRequestPage_Xpath = "//*[contains(@id,'ct100_cphContent_txtPhone')]";
	public static final String SelectRequestType_DropDown_SubmitRequestPage_Xpath = "//*[contains(@id,'ct100_cphContent_drpReason')]";
	public static final String OrderNo_TxtBox_SubmitRequestPage_Xpath = "//*[contains(@id,'ct100_cphContent_txtOrderNo')]";
	public static final String Message_TxtBox_SubmitRequestPage_XPath = " //*[contains(@id,'ct100_cphContent_txtNote')]";
	public static final String IamNotARobot_ChkBox_SubmitRequest_Xpath = "//*[contains(@class,'recapcha-checkbox')]";
	public static final String Submit_Btn_SubmitRequestPage_Xpath = "//*[contains(@id,'ct100_cphContent_btnCreate')]";

	
	
	private  static WebElement element = null;

  public static WebElement SignIn(WebDriver driver)
	  {
	 element = driver.findElement(By.xpath(SIGN_IN_IMG_XPATH));
	 return element;
	    }

   public static WebElement ClickSignIn_Link(WebDriver driver)
    {
     element = driver.findElement(By.linkText(SIGNIN_LINK_LINKTEXT));
     return element;
    }
  
   public static WebElement EmailAddress_TxtBox(WebDriver driver)
    {
   element = driver.findElement(By.xpath(EMAILADDRESS_XPATH));
   return element;
    }
  
   public static WebElement Password_TxtBox(WebDriver driver)
   {
    element = driver.findElement(By.xpath(PASSWORD_XPATH));
    return element;

   }
   public static WebElement SignInBtn(WebDriver driver)
   {
    element = driver.findElement(By.xpath(SIGNIN_BTN_XPATH ));
    return element;

   }

   public static WebElement SubmitARequest(WebDriver driver)
   {
    element = driver.findElement(By.linkText(SubmitARequest_Link_LinkText));
    return element;
   }
   
   public static WebElement SubmitARequest_Email(WebDriver driver)
   {
    element = driver.findElement(By.xpath(Email_TxtBox_SubmitRequestPage_Xpath));
    return element;
   }

   
   
   
   
   
   
}




	