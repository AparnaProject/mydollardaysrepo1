package com.MyDollarDaysTest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.MyDollarDays.CommonMethods.*;
import com.MyDollarDays.Weblocators.*;
import org.apache.log4j.Logger;

public class SubmitARequestPage {
	WebDriver driver = null;
	public static Logger Log = Logger.getLogger(SubmitARequestPage.class.getName());
	
@Test(priority = 1)
	public void SignIn() throws IOException 
{
	MyDollarDaysWeblocators.SignIn(driver).click();;
	MyDollarDaysWeblocators.ClickSignIn_Link(driver).click();	
	MyDollarDaysWeblocators.EmailAddress_TxtBox(driver).click();
    MyDollarDaysWeblocators.EmailAddress_TxtBox(driver).sendKeys(MyDollarDaysCommonMethods.Prop.getProperty("Email_Address")); 
    Log.info("User has entered valid Email Address");
	 
	MyDollarDaysWeblocators.Password_TxtBox(driver).click();
    MyDollarDaysWeblocators.Password_TxtBox(driver).sendKeys(MyDollarDaysCommonMethods.Prop.getProperty("Password")); 	
    Log.info("User has entered valid password");
    
    MyDollarDaysWeblocators.SignInBtn(driver).click();	
	Log.info("User has Signed In");
	MyDollarDaysCommonMethods.captureScreenShot(driver,"User_SignedIn","TC1_UserSignedIn_Screenshot");
	}
@Test(priority = 2)
public void SubmitARequest() 
{
	MyDollarDaysWeblocators.SignIn(driver).click();
	MyDollarDaysWeblocators.SubmitARequest(driver).click();
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	MyDollarDaysWeblocators.SubmitARequest_Email(driver).click();
	
}


@BeforeTest
	public void loadBefore() throws IOException {
		try {
			MyDollarDaysCommonMethods commonMethods = new MyDollarDaysCommonMethods();
			driver = commonMethods.OpeningWebsite();
			Log.info("Property file  is loaded");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
